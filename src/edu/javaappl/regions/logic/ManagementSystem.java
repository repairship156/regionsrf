/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.javaappl.regions.logic;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.SwingUtilities;

/**
 *
 * @author Владимир
 */

public class ManagementSystem  {
    
    private List <Type> types;
    private Collection<Region> regions;
    
    private static ManagementSystem instance;
    
    private ManagementSystem(){
        loadTypes();
        loadRegions();
       
    }

        
    public static synchronized ManagementSystem getInstance(){
        if (instance == null){
            instance = new ManagementSystem();
        }
        return instance;
    }
    
    public static void main(String[] args) {
        try {
            System.setOut(new PrintStream("outReg.txt"));
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();   
            return;
        }
        
        ManagementSystem ms = ManagementSystem.getInstance();
        
        printString("Полный список типов");
        printString("************************************");
        List<Type> allTypes = ms.getTypes();
        for (Type ty : allTypes){
            printString(ty);
        }
        printString();
    
    
    printString("Полный список регионов");
    printString("************************************");
    Collection<Region> allRegions = ms.getAllRegions();
    for(Region re : allRegions){
        printString(re);
}
    printString();
    }
    

    public void loadTypes(){
        if(types == null){
            types = new ArrayList<Type>();
        }
            else {
                types.clear();
            }
            Type t = null;
            
            t = new Type();
            t.setId(1);
            t.setNameType("Республика.");
            types.add(t);
            
            t = new Type();
            t.setId(2);
            t.setNameType("Край.");
            types.add(t);
            
            t = new Type();
            t.setId(3);
            t.setNameType("Область.");
            types.add(t);
            
            t = new Type();
            t.setId(4);
            t.setNameType("Город федерального значения.");
            types.add(t);
    }
    
    public void loadRegions (){
        if (regions == null) {
            regions = new TreeSet<Region>();
        }
        else {
            regions.clear();
        }
        
        Region r = null;
        r = new Region();
        r.setId(1);
        r.setName("Адыгея");
        r.setCapital("Майкоп");
        r.setCode("01");            
        r.setType_id(1);
        regions.add(r);
        
        r = new Region();
        r.setId(2);
        r.setName("Алтай");
        r.setCapital("Горно-Алтайск");
        r.setCode("02");            
        r.setType_id(1);
        regions.add(r);
        
        r = new Region();
        r.setId(3);
        r.setName("Башкортостан");
        r.setCapital("Уфа");
        r.setCode("03");            
        r.setType_id(1);
        regions.add(r);
        
        r = new Region();
        r.setId(4);
        r.setName("Бурятия");
        r.setCapital("Улан-Удэ");
        r.setCode("04");            
        r.setType_id(1);
        regions.add(r);
        
        r = new Region();
        r.setId(5);
        r.setName("Дагестан");
        r.setCapital("Махачкала");
        r.setCode("05");            
        r.setType_id(1);
        regions.add(r);
        
        r = new Region();
        r.setId(6);
        r.setName("Ингушетия");
        r.setCapital("Магас");
        r.setCode("06");            
        r.setType_id(1);
        regions.add(r);
        
        r = new Region();
        r.setId(7);
        r.setName("Кабардино-Балкария");
        r.setCapital("Нальчик");
        r.setCode("07");            
        r.setType_id(1);
        regions.add(r);
        
        r = new Region();
        r.setId(8);
        r.setName("Калмыкия");
        r.setCapital("Элиста");
        r.setCode("08");            
        r.setType_id(1);
        regions.add(r);
        
        r = new Region();
        r.setId(9);
        r.setName("Карачаево-Черкессия");
        r.setCapital("Черкесск");
        r.setCode("09");            
        r.setType_id(1);
        regions.add(r);
        
        r = new Region();
        r.setId(10);
        r.setName("Карелия");
        r.setCapital("Петрозаводск");
        r.setCode("10");            
        r.setType_id(1);        
        regions.add(r);
    }
    
    public List<Type> getTypes() {
        return types;
    }
    
    public Collection<Region> getAllRegions(){
        return regions;
    }
    
    public static void printString(Object o){
        //System.out.println(o.toString());        
        try{
            System.out.println(new String(o.toString().getBytes("windows-1251"), "windows-1251"));
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    
    public static void printString(){
        System.out.println();
    }
}
