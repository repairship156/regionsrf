/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.javaappl.regions.logic;

/**
 *
 * @author Владимир
 */

public class Type {
    private int id;
    private String nameType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }
    
    public String toString(){
        return nameType;
    }
    
}
